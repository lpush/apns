PACKAGE=apns
DIST_DIR=dist
EBIN_DIR=ebin
PRIV_DIR=priv
INCLUDE_DIRS=include
RABBITMQ_HOME ?= /Users/kwon/Documents/workspace/svn_naver_dev001/dev001/tags/ebs/release/xpn_server-1.0.0
RELEASABLE=true

all: compile

clean:
	rm -rf $(DIST_DIR)
	rm -rf $(EBIN_DIR)

distclean: clean
	rm -rf $(DEPS_DIR)

package: compile
	rm -f $(DIST_DIR)/$(PACKAGE).ez
	mkdir -p $(DIST_DIR)/$(PACKAGE)/$(PRIV_DIR)
	cp -r $(EBIN_DIR) $(DIST_DIR)/$(PACKAGE)
	cp -r $(PRIV_DIR) $(DIST_DIR)/$(PACKAGE)
	$(foreach EXTRA_DIR, $(INCLUDE_DIRS), cp -r $(EXTRA_DIR) $(DIST_DIR)/$(PACKAGE);)
	(cd $(DIST_DIR); zip -x '*.svn' -x '*svn*' -x '*.erl' -x '*git*' -x '*rebar*' -r $(PACKAGE).ez $(PACKAGE);)

install: package
	cp $(DIST_DIR)/$(PACKAGE).ez $(RABBITMQ_HOME)/plugins

$(DEPS_DIR):
	./rebar get-deps

compile: $(DEPS_DIR)
	./rebar compile
